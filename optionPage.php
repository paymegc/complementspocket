<?php
add_action( 'admin_menu', 'CPSP_option_page_add_menu' );
function CPSP_option_page_add_menu() {
	add_menu_page(
		'CPSP_option', // page <title>Title</title>
		'Spocket Tool', // menu link text
		'manage_options', // capability to access the page
		'CPSP_option-slug', // page URL slug
		'CPSP_function_option_page', // callback function /w content
		'dashicons-admin-tools', // menu icon
		57.9
	);
 
}
add_action( 'admin_init',  'CPSP_option_page_register_setting' );
 
function CPSP_option_page_register_setting(){
	register_setting(
		'CPSP_option_settings', // settings group name
		'input_CPSP_option_settings', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_section(
		'CPSP_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'CPSP_option-slug' // page slug
	);
 
	add_settings_field(
		'input_CPSP_option_settings',
		'input_CPSP_option_settings',
		'CPSP_option_text_field_html', // function which prints the field
		'CPSP_option-slug', // page slug
		'CPSP_option_settings_section_id', // section ID
		array( 
			'label_for' => 'input_CPSP_option_settings',
			'class' => 'misha-class', // for <tr> element
		)
    );
}
function CPSP_option_text_field_html(){
 
	$text = get_option( 'input_CPSP_option_settings' );
 
	printf(
		'<input type="text" id="input_CPSP_option_settings" name="input_CPSP_option_settings" value="%s" />',
		esc_attr( $text )
	);
 
}
function CPSP_function_option_page(){
    $options = get_option( 'input_CPSP_option_settings' );
    if($options == "")
        $options = "{}";
    $options = json_decode($options,true);

    $checked = $options["checkboxImportProduct"];
    echo $checked;
    ?> 
    <div class="wrap">
        <h1>
            Spocket Tool
        </h1>
        <p class="pInfo">
            If you want to add products through Spockect import, activate the option to differentiate products, in this way the system will be able to differentiate between the products uploaded with this option activated and deactivated
        </p>
        <p class="pInfo">
            <strong>
                It is very important that after important the products deactivate the option
            </strong>
        </p>
        <input 
        type="checkbox" 
        id="checkboxImportProduct"
        name="checkboxImportProduct"
        <?=($checked=="yes")?"checked":""?>
        />
        <label for="checkboxImportProduct"></label>
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'CPSP_option_settings' ); 
                do_settings_sections( 'CPSP_option-slug' ); 
                submit_button();
            ?>
	    </form>
        <style>
            #checkboxImportProduct,
            .misha-class{
                display:none;
            }
            [for="checkboxImportProduct"]{
                display:flex;
                --size:30px;
                width:calc(20px + calc(var(--size) * 2));
                background:  #23282d;
                padding:10px;
                --check:red;
                border-radius:45px;
            }
            [for="checkboxImportProduct"]:before{
                content:"";
                display:block;
                width:var(--size);
                height:var(--size);
                border-radius:100%;
                background:var(--check,red);
            }
            #checkboxImportProduct:checked + [for="checkboxImportProduct"]{
                --check:green;
                justify-content: flex-end;
            }
            .pInfo{
                font-size: 20px;
            }
        </style>
        <script>
            input = document.getElementById('input_CPSP_option_settings')
            options = input.value
            if(options == "")
                options = "{}"
            options = JSON.parse(options)

            checkbox = document.getElementById('checkboxImportProduct')
            checkbox.onclick = () => {
                options.checkboxImportProduct = checkbox.checked
            }

            submit = document.getElementById('submit')
            submit.onclick = () =>{
                input.value = JSON.stringify(options)
            }
        </script>
    </div>
    <?php
}