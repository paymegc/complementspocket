<?php
function CPSP_newProduct( $id ) { 
    $options = get_option( 'input_CPSP_option_settings' );
    if($options == "")
        $options = "{}";
    $options = json_decode($options,true);

    $checked = $options["checkboxImportProduct"];
    if($checked=="yes")
        add_post_meta( $id, 'ProductSpocket', 'yes', true ); 
};
add_action( 'woocommerce_new_product', 'CPSP_newProduct', 10, 3 ); 
add_action( 'woocommerce_update_product', 'CPSP_newProduct', 10, 3 ); 