<?php
/*
Plugin Name: ComplementSpocket
Plugin URI: https://startscoinc.com/es/
Description: Complemento de woocommerce con los app.spocket
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/*
License...
Copyright 2020 Startsco, Inc.
*/

//prefit CPSP
define('PREFITCPSP', "CPSP");

if ( ! function_exists( 'is_plugin_active' ) )
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

if(is_plugin_active( 'spocket/index.php' )){
    require_once plugin_dir_path( __FILE__ ) . './hookAddProduct.php';
    require_once plugin_dir_path( __FILE__ ) . './optionPage.php';
}else{
    function CPSP_log_dependencia() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p>
                ComplementSpocket requiere que el plugin Spocket este Activado
            </p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'CPSP_log_dependencia' );
}